require 'rails_helper'

RSpec.describe Comment, type: :model do
  current_user = User.first_or_create!(name:'pedro',email: 'pedro@example.com',password:'123456', password_confirmation:'123456')
  current_post = Post.first_or_create!(content_text:'ok',user_id:1)
  it 'has a title' do
    post= Comment.new(
      content_text:'hello world',
      post: current_post,
      user: current_user
    )
    expect(post).to be_valid
  end
end