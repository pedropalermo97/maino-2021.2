require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has created' do
    user = User.new(
      name:'pedro', 
      email: 'pedro@example.com',
      password: '123456',
      password_confirmation: '123456'
    )
    expect(user).to be_valid
  end 

  it 'has not created' do
    user = User.new(
      name:'pedro', 
      password: '123456',
      password_confirmation: '123456'
    )
    expect(user).to_not be_valid
  end 
end
