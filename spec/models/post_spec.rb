require 'rails_helper'

RSpec.describe Post, type: :model do
  current_user = User.first_or_create!(name:'pedro',email: 'pedro@example.com',password:'123456', password_confirmation:'123456')
  it 'has a title' do
    post= Post.new(
      content_text:'hello world',
      user: current_user
    )
    expect(post).to be_valid
  end
end