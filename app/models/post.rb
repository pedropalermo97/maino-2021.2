class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :content_text, presence: true, length: {maximum: 256}
  
end
