class User < ApplicationRecord
  has_secure_password
  ##Relations
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  ##Validates
  validates :name, :email, presence: true, uniqueness: true
  validates :password, :password_confirmation, length: {minimun: 7}, length: {maximum: 12}
end
