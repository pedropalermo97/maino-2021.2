Rails.application.routes.draw do
  get 'pages/homepage'
  root to: "pages#homepage"
  
  resources :users
  post 'auth/login'
  post 'auth/signup'

  post 'users', to: "users#create"
  get 'users', to: "users#index"
  get 'users/:id', to: "users#show"
  put'users/:id', to: "users#update"
  delete 'users/:id', to: "users#destroy"
  
  get 'posts/:id', to: "posts#show"
  put 'posts/:id', to: "posts#update"
  delete 'posts/:id', to: "posts#destroy"
  post 'posts', to: "posts#create"
  get 'posts', to: "posts#index"

  get 'comments/:id', to: "comments#show"
  put'comments/:id', to: "comments#update"
  delete 'comments/:id', to: "comments#destroy"
  post 'comments', to: "comments#create"
  get 'comments', to: "comments#index"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
